///<reference types="Cypress"/>

it('get Test',()=>{
    cy.viewport(1800, 1000)
    cy.visit('https://autoreview.ru/')
    cy.get('a').contains('подписка')
})

it('get with find',()=>{
    cy.viewport(1600, 1200)
    cy.visit('https://docs.cypress.io/guides/overview/why-cypress#What-you-ll-learn')
    cy.get('aside').find('div').find('button').eq(2)
})
it('get with contains',()=> {
    cy.viewport(1600, 1200)
    cy.visit('https://docs.cypress.io/guides/overview/why-cypress#What-you-ll-learn')
    cy.get('button').contains('Component Testing')
})

it('contains exception',()=>{
    cy.visit('https://www.audi.ru/ru/web/ru/model-range.html')
    cy.contains('span','УЗНАТЬ больше',{matchCase:false})
})

it.only('contains',()=> {
    cy.viewport(1600, 1000)
    cy.visit('https://autoreview.ru/')
    cy.get('footer').contains('реКЛама', {matchCase: false})
})