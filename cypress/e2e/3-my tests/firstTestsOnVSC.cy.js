// it('by id',()=>{
//     cy.visit('https://autoreview.ru/')
//     cy.get('#search')
// })

// it('by class',()=>{
//     cy.visit('https://autoreview.ru/')
//     cy.get('.butt')
// })

// it('by tag 2',()=>{
//     cy.visit('https://docs.cypress.io/api/table-of-contents')
//     cy.get('nav')
// })

// it('by tag', ()=>{
//     cy.visit('https://docs.cypress.io/api/table-of-contents');
//     cy.get('nav');
// });

// it('by tag value',()=>{
//     cy.visit('https://vk.com/')
//     cy.get('[name="login"]')
// })

// it('by tag value 2',()=>{

//     cy.visit('https://vk.com/?lang=en')
//     cy.get('[class="VkIdForm__input"]')
// })

// it('by different tag',()=>{
//     cy.visit('https://vk.com/?lang=en')
//     cy.get('[id="index_email"][placeholder="Phone or email"]')
// })

// it('by different types',()=>{
//     cy.visit('https://docs.cypress.io/api/table-of-contents')
//     cy.get('.DocSearch[type="button"]')
// })

// it('by different types 2',()=>{
//     cy.visit('https://vk.com/?lang=en')
//     cy.get('button[type="button"][class="FlatButton FlatButton--positive FlatButton--size-l FlatButton--wide VkIdForm__button VkIdForm__signUpButton"]')
// })

// it.only('by contains name',()=>{
//     cy.visit('https://vk.com/?lang=en')
//     cy.get('*[id^="wrap_bet"]')
// })


it('UsingGetWithFindAndEq',()=>{
    cy.viewport(1800, 1200)
    cy.visit('https://docs.cypress.io/api/events/catalog-of-events')
    cy.get('div').find('nav').find('ul').find('li').eq(0)
})

it('usingGetWithFindAndEq',()=>{
    cy.viewport(1800, 1200);
    cy. visit('https://docs.cypress.io/api/events/catalog-of-events#Event-Types')
    cy.get('aside').find('div').find('button').eq(4).find('svg').find('path')
})

it.only('get with find and eq',()=>{
    cy.viewport(1800, 1200)
    cy.visit('https://docs.cypress.io/api/cypress-api/custom-commands')
    cy.get('nav').find('ul').find('li').eq(1)
})